# Simple rFactor 2 Server List

A very simple server list for rFactor 2 game. Can be probably used with other games OR adjusted, if needed.

## Usage

1. Get these 2 PHP files and upload them to your server. You can rename "rF2.php" if you wish.
2. Edit "SteamAPI.php":
    1. Change the Steam API key. You can get yours from [here - Steam WebAPI](https://steamcommunity.com/dev/apikey "Steam Web API Key").
    2. Edit the Limit to your liking, or leave it as it is.
3. That's it, now open that webpage in your browser and you should have your own Server List!

## Editing the Webpage

Editing the webpage should be straightforward, as the webpage itself is made in HTML with some small additions of PHP and tiny bits of CSS and Javascript. Feel free to make a better version of the page if you wish - this is only a "proof of concept", to show how it works.

## Can I use it with other games?

Probably, if the game supports Steam Server List. Just check the "filter" variable in "SteamAPI.php" file (check out the comment).

Enjoy!