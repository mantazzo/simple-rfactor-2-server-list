<!-- Let's not forget including the SteamAPI generator - it's the main thing here -->
<? include("SteamAPI.php");?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Simple rFactor 2 Server List</title>  <!-- Webpage Title -->
        <meta charset="UTF-8">
        <meta name="description" content="A very simple webpage which shows full rFactor 2's Server List in a simple way.">  <!-- SEO stuff -->
        <meta name="author" content="Mantazzo">
        <meta name="keywords" content="rFactor 2, rFactor, rF2, rF, server list, serverlist, servers, simple server list">
        <meta http-equiv="refresh" content="300">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  <!-- Adding "Responsive" tag to have fluid size of the webpage - works for any screen with this -->
        <!-- Now, let's add some style - borders for the table, and making the text in table cells centered -->
        <style>
        table, th, td {
            border: 1px solid black;
        }
        td {
         text-align: center;
        }
        </style>
        <!-- Time for a simple script - Showing different tables, while hiding others. Very basic, but hey, it works. -->
        <script>
            function change_table_normal () {
                t1 = document.getElementById("serverlist");
                t2 = document.getElementById("serverlist-players");
                t3 = document.getElementById("serverlist-bots");
                t4 = document.getElementById("serverlist-maxplayers");
                t5 = document.getElementById("serverlist-version");

                t1.style.display = 'table';
                t2.style.display = 'none';
                t3.style.display = 'none';
                t4.style.display = 'none';
                t5.style.display = 'none';
            }
            function change_table_players () {
                t1 = document.getElementById("serverlist");
                t2 = document.getElementById("serverlist-players");
                t3 = document.getElementById("serverlist-bots");
                t4 = document.getElementById("serverlist-maxplayers");
                t5 = document.getElementById("serverlist-version");

                t1.style.display = 'none';
                t2.style.display = 'table';
                t3.style.display = 'none';
                t4.style.display = 'none';
                t5.style.display = 'none';
            }
            function change_table_bots () {
                t1 = document.getElementById("serverlist");
                t2 = document.getElementById("serverlist-players");
                t3 = document.getElementById("serverlist-bots");
                t4 = document.getElementById("serverlist-maxplayers");
                t5 = document.getElementById("serverlist-version");

                t1.style.display = 'none';
                t2.style.display = 'none';
                t3.style.display = 'table';
                t4.style.display = 'none';
                t5.style.display = 'none';
            }
            function change_table_maxplayers () {
                t1 = document.getElementById("serverlist");
                t2 = document.getElementById("serverlist-players");
                t3 = document.getElementById("serverlist-bots");
                t4 = document.getElementById("serverlist-maxplayers");
                t5 = document.getElementById("serverlist-version");

                t1.style.display = 'none';
                t2.style.display = 'none';
                t3.style.display = 'none';
                t4.style.display = 'table';
                t5.style.display = 'none';
            }
            function change_table_version () {
                t1 = document.getElementById("serverlist");
                t2 = document.getElementById("serverlist-players");
                t3 = document.getElementById("serverlist-bots");
                t4 = document.getElementById("serverlist-maxplayers");
                t5 = document.getElementById("serverlist-version");

                t1.style.display = 'none';
                t2.style.display = 'none';
                t3.style.display = 'none';
                t4.style.display = 'none';
                t5.style.display = 'table';
            }
        </script>
    </head>
    <body>
        <!-- Initial gap and some text for better readability. -->

        <h1>Simple public rFactor 2's server list</h1>
        <p>This is a very simple list of the rFactor 2's server list, or at least what's available publicly (in the Steam Game Servers Browser).</p>
        <br>
        <p>Sorting:
        <br>
        <i>Normal is used as default.</i></p> <!-- In this case, normal is pretty much how the list is given by Steam API. Using buttons will show different tables which use column sorts. -->
        <p><button onclick="change_table_normal()">Normal</button>   <button onclick="change_table_players()">By Players</button>   <button onclick="change_table_bots()">By Bots</button>   <button onclick="change_table_maxplayers()">By Max Players</button>   <button onclick="change_table_version()">By Build</button></p>
        <br>
        <p>The full server list is as follows:
        <br>
        <i>Note: Players - Players Online, Max - Maximum amount of players in the server. The list does not return if the server is password protected or not.</i></p> <!-- Unfortunately for rFactor 2 no "password-protection" type is reported in any way using WebAPI. -->
        <table style="width:100%; display: table;" id="serverlist">
            <!-- Generating the "Default" table. Shown by default.-->
            <tr>
                <th>ID</th>
                <th>Server Name</th>
                <th>Server IP</th>
                <th>Server Port</th>
                <th>Players</th>
                <th>Bots</th>
                <th>Max</th>
                <th>Current Track</th>
                <th>Game Build</th>
                <th colspan="2">Connect</th>
            </tr>
            <?php
                /** Generating Servers Output.
                 * This is not the nicest way nor it's most effective (probably) but it works and it works correctly. Fight me.
                 */
                for ($i=0; ; $i++)
                {
                    if ($i == $number)  // I guess I could have set up the cycle "$i < $number", but I guess I just wanted to make sure I get ALL servers. Anyway, this works.
                    {
                        break;
                    }

                    $id = $i+1;  // Let's set the ID to be +1 bigger - we don't want to see ID 0 on the list as it doesn't look nice, though the list starts from 0

                    $tempip = $json["response"]["servers"][$i]["addr"];  // Temporary variable for IP as the list provides wrong address (IP)
                    $ip = strstr($tempip, ':', true) ?: $tempip;  // We need ONLY the IP, so let's remove the "port"

                    $connectip = $ip.":".$json["response"]["servers"][$i]["gameport"];  // Now let's use the IP we got and make the correct connection IP
                    $steamconnect = "steam://run/365960//+connect ".$connectip;  // Let's make a Steam link to launch the game directly from Steam
                    $cmdconnect = "CMD: +multiplayer +connect ".$connectip;  // Additionally, let's do a setup for CMD Line (Command Line) usage
                    //steam://run/365960//+connect xxx.xxx.xxx.xxx:yyyyy is how Steam handles connections for rFactor 2, where the xxx.xxx.xxx.xxx:yyyyy is IP:Port
                    
                    $ver = substr($json["response"]["servers"][$i]["version"], 1);  // Version reports 5 digits, we only need last 4 as the first digit is always the same for now
                    // Time to output all this "monstrosity"
                    echo '<tr>';  // Make a new row
                    echo "<td>".$id."</td>";  // Start with ID
                    echo "<td>".$json["response"]["servers"][$i]["name"]."</td>";  // Grab and show the name of the Server
                    echo "<td>".$ip."</td>";  // Show the IP
                    echo "<td>".$json["response"]["servers"][$i]["gameport"]."</td>";  // Grab and show the port you need to actually use
                    echo "<td>".$json["response"]["servers"][$i]["players"]."</td>";  // Grab and show how many players are on the server
                    echo "<td>".$json["response"]["servers"][$i]["bots"]."</td>";  // Grab and show how many bots (AI) are on the server
                    echo "<td>".$json["response"]["servers"][$i]["max_players"]."</td>";  // Grab and show the limit of players in total
                    echo "<td>".$json["response"]["servers"][$i]["map"]."</td>";  // Grab and show the current map which is reported
                    echo "<td>".$ver."</td>";  // Show the game version
                    echo '<td><a href="'.$steamconnect.'">STEAM LINK</a></td>';  // Make a Steam Link - press it to launch the game and connect to server
                    echo "<td>".$cmdconnect."</td>";  // Show the manual way of connection
                    echo "</tr>";  // End the row
                }
            ?>
        </table>
        <table style="width:100%; display: none;" id="serverlist-players">
            <!-- Generating the "Players" table - sorted by player amount, most players on top. Hidden by default.-->
            <tr>
                <th>ID</th>
                <th>Server Name</th>
                <th>Server IP</th>
                <th>Server Port</th>
                <th>Players</th>
                <th>Bots</th>
                <th>Max</th>
                <th>Current Track</th>
                <th>Game Build</th>
                <th colspan="2">Connect</th>
            </tr>
            <?php
            /** This generation is a bit different as we need to find servers with biggest player amounts.
             * First, we need to find the biggest amount from the server list, and work our way down.
             */
                $play = 0;  // The variable is used for finding the biggest amount of players in the list
                for ($i = 0; $i < $number; $i++) {  // Time to check the list
                    if ($json["response"]["servers"][$i]["players"] > $play) {  // If the number is bigger...
                        $play = $json["response"]["servers"][$i]["players"];  // ..save it
                    }
                }
                while ($play >= 0) {  // We're doing from most players to no players, so let's not go under 0
                    for ($i = 0; $i < $number; $i++) {  // Fixed the check here and in the remaining checks, hehe
                        if ($json["response"]["servers"][$i]["players"] == $play) {  // Select only the server data which has that player amount and output it
                            $id = $i+1;
                            $tempip = $json["response"]["servers"][$i]["addr"];
                            $ip = strstr($tempip, ':', true) ?: $tempip;
                            $connectip = $ip.":".$json["response"]["servers"][$i]["gameport"];
                            $steamconnect = "steam://run/365960//+connect ".$connectip;
                            $cmdconnect = "CMD: +multiplayer +connect ".$connectip;
                
                            $ver = substr($json["response"]["servers"][$i]["version"], 1);
                            echo '<tr>';
                            echo "<td>".$id."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["name"]."</td>";
                            echo "<td>".$ip."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["gameport"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["players"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["bots"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["max_players"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["map"]."</td>";
                            echo "<td>".$ver."</td>";
                            echo '<td><a href="'.$steamconnect.'">STEAM LINK</a></td>';
                            echo "<td>".$cmdconnect."</td>";
                            echo "</tr>";
                        }
                    }
                    $play--;  // Reduce the number by 1 when there are no more servers with that player amount, continue the "while" cycle while we can
                }
            ?>
        </table>
        <table style="width:100%; display: none;" id="serverlist-bots">
            <!-- Generating the "Bots" table - sorted by bots (AI) amount, servers with most bots on top. Hidden by default.-->
            <tr>
                <th>ID</th>
                <th>Server Name</th>
                <th>Server IP</th>
                <th>Server Port</th>
                <th>Players</th>
                <th>Bots</th>
                <th>Max</th>
                <th>Current Track</th>
                <th>Game Build</th>
                <th colspan="2">Connect</th>
            </tr>
            <?php
                /** Same check as with "players" table, but we're now using different field (bots).
                 * I am not going to comment here too much as the thing is literally the same.
                 */
                $bots = 0;  // Variable for Bots amount
                for ($i = 0; $i < $number; $i++) {
                    if ($json["response"]["servers"][$i]["bots"] > $bots) {  // If the amount is bigger than the $bots variable...
                        $bots = $json["response"]["servers"][$i]["bots"];  // ..overwrite it
                    }
                }
                while ($bots >= 0) { // same checks as with "players" table
                    for ($i = 0; $i < $number; $i++) {
                        if ($json["response"]["servers"][$i]["bots"] == $bots) {
                            $id = $i+1;
                            $tempip = $json["response"]["servers"][$i]["addr"];
                            $ip = strstr($tempip, ':', true) ?: $tempip;
                            $connectip = $ip.":".$json["response"]["servers"][$i]["gameport"];
                            $steamconnect = "steam://run/365960//+connect ".$connectip;
                            $cmdconnect = "CMD: +multiplayer +connect ".$connectip;
                
                            $ver = substr($json["response"]["servers"][$i]["version"], 1);
                            echo '<tr>';
                            echo "<td>".$id."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["name"]."</td>";
                            echo "<td>".$ip."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["gameport"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["players"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["bots"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["max_players"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["map"]."</td>";
                            echo "<td>".$ver."</td>";
                            echo '<td><a href="'.$steamconnect.'">STEAM LINK</a></td>';
                            echo "<td>".$cmdconnect."</td>";
                            echo "</tr>";
                        }
                    }
                    $bots--; // reduce the "bots" amount by 1 and continue on while we can
                }
            ?>
        </table>
        <table style="width:100%; display: none;" id="serverlist-maxplayers">
            <!-- Generating the "Max Players" table - sorted by Maximum Players amount, biggest servers at the top. Hidden by default.-->
            <tr>
                <th>ID</th>
                <th>Server Name</th>
                <th>Server IP</th>
                <th>Server Port</th>
                <th>Players</th>
                <th>Bots</th>
                <th>Max</th>
                <th>Current Track</th>
                <th>Game Build</th>
                <th colspan="2">Connect</th>
            </tr>
            <?php
            /** Same checks as with "players" and "bots" tables. */
                $maxp = 0; // Variable for Max Players amount
                for ($i = 0; $i < $number; $i++) {
                    if ($json["response"]["servers"][$i]["max_players"] > $maxp) {
                        $maxp = $json["response"]["servers"][$i]["max_players"];
                    }
                }
                while ($maxp >= 0) {  // could probably change the number to 1 because 0 max players doesn't make sense, but I guess just in case...
                    for ($i = 0; $i < $number; $i++) {
                        if ($json["response"]["servers"][$i]["max_players"] == $maxp) {
                            $id = $i+1;
                            $tempip = $json["response"]["servers"][$i]["addr"];
                            $ip = strstr($tempip, ':', true) ?: $tempip;
                            $connectip = $ip.":".$json["response"]["servers"][$i]["gameport"];
                            $steamconnect = "steam://run/365960//+connect ".$connectip;
                            $cmdconnect = "CMD: +multiplayer +connect ".$connectip;
                
                            $ver = substr($json["response"]["servers"][$i]["version"], 1);
                            echo '<tr>';
                            echo "<td>".$id."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["name"]."</td>";
                            echo "<td>".$ip."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["gameport"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["players"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["bots"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["max_players"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["map"]."</td>";
                            echo "<td>".$ver."</td>";
                            echo '<td><a href="'.$steamconnect.'">STEAM LINK</a></td>';
                            echo "<td>".$cmdconnect."</td>";
                            echo "</tr>";
                        }
                    }
                    $maxp--;  // Reduce the number and carry on with the generation
                }
            ?>
        </table>
        <table style="width:100%; display: none" id="serverlist-version">
            <!-- Generating the "Version" table - sorted by Game Version, highest version (usually current game version) on top.. Hidden by default.-->
            <tr>
                <th>ID</th>
                <th>Server Name</th>
                <th>Server IP</th>
                <th>Server Port</th>
                <th>Players</th>
                <th>Bots</th>
                <th>Max</th>
                <th>Current Track</th>
                <th>Game Build</th>
                <th colspan="2">Connect</th>
            </tr>
            <?php
            /** Standard checks with a small twist. */
                $build = 0; // Variable for biggest game build number (usually current version)
                for ($i = 0; $i < $number; $i++) {
                    if ($json["response"]["servers"][$i]["version"] > $build) {
                        $build = $json["response"]["servers"][$i]["version"];
                    }
                }
                while ($build >= 11100) {  // slightly different check - at this point there are no servers running lower builds than 1.1100, so I can save some time here. Potential change in the future to increase minimum build number.
                    for ($i = 0; $i < $number; $i++) {
                        if ($json["response"]["servers"][$i]["version"] == $build) {
                            $id = $i+1;
                            $tempip = $json["response"]["servers"][$i]["addr"];
                            $ip = strstr($tempip, ':', true) ?: $tempip;
                            $connectip = $ip.":".$json["response"]["servers"][$i]["gameport"];
                            $steamconnect = "steam://run/365960//+connect ".$connectip;
                            $cmdconnect = "CMD: +multiplayer +connect ".$connectip;
                
                            $ver = substr($json["response"]["servers"][$i]["version"], 1);
                            echo '<tr>';
                            echo "<td>".$id."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["name"]."</td>";
                            echo "<td>".$ip."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["gameport"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["players"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["bots"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["max_players"]."</td>";
                            echo "<td>".$json["response"]["servers"][$i]["map"]."</td>";
                            echo "<td>".$ver."</td>";
                            echo '<td><a href="'.$steamconnect.'">STEAM LINK</a></td>';
                            echo "<td>".$cmdconnect."</td>";
                            echo "</tr>";
                        }
                    }
                    $build--;  // Reduce Build number and carry on generating the rest of it
                }
            ?>
        </table>
        <br>
        <br>
        <!-- Finally, some comments at the end, because why not. Can be removed freely. -->
        <h3>That's a loooong list...</h3>
        <h3>Made by mantazzo, 2020-2021.</h3>
    </body>
</html>