<?php
/** Keep in mind that this file should not be accessed directly.
 * However, it should not cause any issues if you access it, as it does not output anything unnecessary.
 */
$api_key = "<steam_API_key_here>";       // Get your Steam API key from https://steamcommunity.com/dev/apikey
$filter = '%5Cappid%5C365960';           // Can replace "365960" for any other compatible Steam AppID (which uses Steam Servers systems)
$limit = 1000;                           // Custom number how many servers to retrieve because usually we don't need ALL servers; number is enough for rF2 to show all servers
$api_url = "https://api.steampowered.com/IGameServersService/GetServerList/v1/?key=$api_key&filter=$filter&limit=$limit&format=json";  // Mandatory - specific URL to work with. "&limit=$limit" part can be removed if you want ALL servers to be shown, but for some games it can be quite a lot.
// Now we need to do some additional stuff
$temp = file_get_contents($api_url);     // Temporary variable as we need conversion
$convert = utf8_encode($temp);           // The thing converts the contents to be UTF-8 compatible, because there are some specific characters which break the standard JSON decoder.
$json = json_decode($convert, true);     // After doing the conversion, JSON decoder works as intended.

if ($json == "")                         // In case the system fails for one reason or another (like Steam downtime, etc)
{
    echo "Technical issues - system failed to retrieve ServerList.\nTry again for a coin?";  // custom message to show on screen
    die();                               // Stop executing completely
}
$number = count($json["response"]["servers"]);  // A small variable for counting how many servers are there in total; should never go over the limit
?>